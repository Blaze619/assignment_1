/*
 *  The only knowledge that can hurt you is the knowledge you don't have.
 */
package Scenario_2;

/**
 *
 * @author Abhik_Blaze
 */
public interface DriverFactory {
    
    public DisplayDriver getDisplayDriver() ;
    public PrintDriver getPrintDriver() ;
    
    
}
