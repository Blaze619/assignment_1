/*
 *  The only knowledge that can hurt you is the knowledge you don't have.
 */
package Scenario_2;

/**
 *
 * @author Abhik_Blaze
 */
public class HighResPrintDriver implements PrintDriver{
    
    @Override
    public String DriverInfo() {
        return "Print Driver Resolution : High";
    }

    @Override
    public void Print(String shape) {
        System.out.println("Found Print Driver.");
        System.out.println(DriverInfo());
        System.out.println("Printed "+ shape); 
    }
    
    
    
}
