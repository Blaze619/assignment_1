/*
 *  The only knowledge that can hurt you is the knowledge you don't have.
 */
package Scenario_2;

/**
 *
 * @author Abhik_Blaze
 */
public class HighResDriverFactory implements DriverFactory{

    @Override
    public DisplayDriver getDisplayDriver() {
        return new HighResDisplayDriver() ;
    }

    @Override
    public PrintDriver getPrintDriver() {
        return new HighResPrintDriver() ;
    }
    
    
}
