/*
 *  The only knowledge that can hurt you is the knowledge you don't have.
 */
package Scenario_2;

/**
 *
 * @author Abhik_Blaze
 */
public class MySystemDemo {
    
    public static void main(String[] args) {
        DriverFactory factory ;
        MySystem sys = new MySystem() ;
        
        System.out.println("Running mySystem on Low End Computer....");
        factory = new LowResDriverFactory() ;
       
        sys.setDrivers(factory);
        System.out.println("Instruction: Dispaly a rectange");
        sys.displayShape("Rectangle");
        System.out.println("Instruction: Print a circle");
        sys.printShape("Circle");
        
        System.out.println("Running mySystem on High End Computer....");
        factory = new HighResDriverFactory() ;
        sys = new MySystem() ;
        sys.setDrivers(factory);
        System.out.println("Instruction: Dispaly a rectange");
        sys.displayShape("Rectangle");
        System.out.println("Instruction: Print a circle");
        sys.printShape("Circle");
        
        
        
        
      
        
    }
}
