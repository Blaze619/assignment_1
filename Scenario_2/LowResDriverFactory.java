/*
 *  The only knowledge that can hurt you is the knowledge you don't have.
 */
package Scenario_2;

/**
 *
 * @author Abhik_Blaze
 */
public class LowResDriverFactory implements DriverFactory{

    @Override
    public DisplayDriver getDisplayDriver() {
        return new LowResDisplayDriver() ;
    }

    @Override
    public PrintDriver getPrintDriver() {
        return new LowResPrintDriver() ;
    }
    
}
