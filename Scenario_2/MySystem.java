/*
 *  The only knowledge that can hurt you is the knowledge you don't have.
 */
package Scenario_2;

/**
 *
 * @author Abhik_Blaze
 */
public class MySystem {
    
    private DisplayDriver display ;
    private PrintDriver print ;
    

    public void setDrivers(DriverFactory factory) {
        display = factory.getDisplayDriver() ;
        print = factory.getPrintDriver() ;
    }
    
    public void displayShape(String shape){
        display.Display(shape);
    }
    
    public void printShape(String shape){
        print.Print(shape);
    }
    
}
