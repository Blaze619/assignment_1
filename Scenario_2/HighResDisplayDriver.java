/*
 *  The only knowledge that can hurt you is the knowledge you don't have.
 */
package Scenario_2;

/**
 *
 * @author Abhik_Blaze
 */
public class HighResDisplayDriver implements DisplayDriver{

    @Override
    public String DriverInfo() {
        return "Display Driver Resolution : High";
    }

    @Override
    public void Display(String shape) {
        System.out.println("Found Display Driver.");
        System.out.println(DriverInfo());
        System.out.println("Displayed "+ shape);   
    }
    
}
