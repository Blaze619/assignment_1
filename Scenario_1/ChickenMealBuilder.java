/*
 *  The only knowledge that can hurt you is the knowledge you don't have.
 */
package Scenario_1;

/**
 *
 * @author Abhik_Blaze
 */
public class ChickenMealBuilder extends MealBuilder{

    @Override
    public void makeMainItem() {
        meal.setMainItem("Chicken");
    }

    @Override
    public void makeSideItem() {
        meal.setSideItem("Noodles");
    }

    @Override
    public void makeToy() {
        meal.setToy("Action Figure");
    }

    @Override
    public void makeDrink() {
        meal.setDrinks("Fanta");
    }
    
}
