/*
 *  The only knowledge that can hurt you is the knowledge you don't have.
 */
package Scenario_1;

import java.util.ArrayList;

/**
 *
 * @author Abhik_Blaze
 */
public class OrderPackage {
    
    private ArrayList<String> bag = new ArrayList<>() ;
    private String cup = null ;
    
    void insertMainItemInBag(String mainItem) {
        bag.add("Main Item: "+ mainItem) ;
    }
    
    void insertSideItemInBag(String sideItem){
        bag.add("Side Item: "+ sideItem) ;
    }
    
    void insertToyInBag(String toy){
        bag.add("Toy: " + toy );
    }
    
    void pourDrinkInCup(String drinks){
        cup = "Drink :" + drinks ;
    }
    
    void showOrderPackage() {
        System.out.println("In bag :");
        for( String s: bag )
            System.out.println(s);
        System.out.println("In Cup: ");
        System.out.println(cup);
        
    }
    
}
