/*
 * The only knowledge that can hurt you is the knowledge you don't have.
 */
package Scenario_1;

/**
 *
 * @author Abhik_Blaze
 */
public abstract class MealBuilder {
    
    protected Meal  meal ;
    
    public void prepareMeal(){
        meal = new Meal() ;
    }
    
    public Meal getMeal(){
        return meal ;
    }
    
    public abstract void makeMainItem() ;
    public abstract void makeSideItem() ;
    public abstract void makeToy() ;
    public abstract void makeDrink() ;
    
}
