/*
 *  The only knowledge that can hurt you is the knowledge you don't have.
 */
package Scenario_1;

/**
 *
 * @author Abhik_Blaze
 */
public class HamburgerMealBuilder extends MealBuilder {

    @Override
    public void makeMainItem() {
        meal.setMainItem("Hamburger");
    }

    @Override
    public void makeSideItem() {
        meal.setSideItem("Fries");
    }

    @Override
    public void makeToy() {
        meal.setToy("Car") ;
    }

    @Override
    public void makeDrink() {
        meal.setDrinks("Pepsi");
    }
    
    
}
