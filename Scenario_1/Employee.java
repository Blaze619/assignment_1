/*
 *  The only knowledge that can hurt you is the knowledge you don't have.
 */
package Scenario_1;

/**
 *
 * @author Abhik_Blaze
 */
public class Employee {
    
    private MealBuilder builder = null ;

    public void setBuilder(MealBuilder builder) {
        this.builder = builder;
    }
    
    public OrderPackage getOrderedPackage() {
        Meal meal = builder.getMeal() ;
        OrderPackage order = new OrderPackage() ;
        
        order.insertMainItemInBag(meal.getMainItem());
        order.insertSideItemInBag(meal.getSideItem());
        order.insertToyInBag(meal.getToy());
        order.pourDrinkInCup(meal.getDrinks());
        
        return order ;
    }
    
    public void placeOrderToBuilder() {
        
        builder.prepareMeal();
        builder.makeMainItem();
        builder.makeSideItem();
        builder.makeToy();
        builder.makeDrink();
        
    }    
}
