/*
 *  The only knowledge that can hurt you is the knowledge you don't have.
 */
package Scenario_1;

/**
 *
 * @author Abhik_Blaze
 */
public class CheeseburgerMealBuilder extends MealBuilder{

    @Override
    public void makeMainItem() {
        meal.setMainItem("Cheeseburger");
    }

    @Override
    public void makeSideItem() {
        meal.setSideItem("Pasta");
    }

    @Override
    public void makeToy() {
        meal.setToy("Plane");
    }

    @Override
    public void makeDrink() {
        meal.setDrinks("Coke");
    }
    
    
    
}
