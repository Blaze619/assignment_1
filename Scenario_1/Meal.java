/*
 * The only knowledge that can hurt you is the knowledge you don't have.
 */
package Scenario_1;

/**
 *
 * @author Abhik_Blaze
 */
public class Meal {
    
    private String mainItem = null ;
    private String sideItem = null ;
    private String toy = null ;
    private String drinks = null ;

    public String getMainItem() {
        return mainItem;
    }

    public String getSideItem() {
        return sideItem;
    }

    public String getToy() {
        return toy;
    }

    public String getDrinks() {
        return drinks;
    }

    public void setMainItem(String mainItem) {
        this.mainItem = mainItem;
    }

    public void setSideItem(String sideItem) {
        this.sideItem = sideItem;
    }

    public void setToy(String toy) {
        this.toy = toy;
    }

    public void setDrinks(String drinks) {
        this.drinks = drinks;
    }
    
    
    
}
