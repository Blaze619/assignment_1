/*
 *  The only knowledge that can hurt you is the knowledge you don't have.
 */
package Scenario_1;

/**
 *
 * @author Abhik_Blaze
 */
public class Restaurant {
    
    public static void main(String[] args) {
        
        Employee em = new Employee() ;
        
        System.out.println("Customer ordered Hamburger");
        MealBuilder builder = new HamburgerMealBuilder() ;
        
        em.setBuilder(builder);
        em.placeOrderToBuilder();
        
        OrderPackage order = em.getOrderedPackage() ;
        order.showOrderPackage();
        
        System.out.println("Customer ordered Cheeseburger");
        builder = new CheeseburgerMealBuilder() ;
        em.setBuilder(builder);
        em.placeOrderToBuilder();
        
        order = em.getOrderedPackage();
        order.showOrderPackage();
        
        System.out.println("Customer ordered Chicken");
        builder = new ChickenMealBuilder() ;
        em.setBuilder(builder);
        em.placeOrderToBuilder();
        
        order = em.getOrderedPackage();
        order.showOrderPackage();
        
        
    }
    
}
